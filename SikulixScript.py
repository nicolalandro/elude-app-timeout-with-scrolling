popup_timeout = 3
popup_message = "Did you want to continue to scroll? (after 3 second without response it )"
popup_title = "Scroll for timeout"

after_popup_delay = 2
next_popup_delay = 2

while(True):
    answer = Do.popAsk(popup_message, popup_title, popup_timeout)
    if not answer and answer != None:
        exit(1)

    sleep(after_popup_delay)
        
    wheel(Mouse.WHEEL_DOWN, 5)
    wheel(Mouse.WHEEL_UP, 5)

    sleep(next_popup_delay)