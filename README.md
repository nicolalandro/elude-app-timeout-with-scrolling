# Elude app timeout with Sikulix Script
If you have to wait a lot on an app that have a timeout, you must scroll some times to not lost the work, with that script you can do it automatically.
It ask a popup, after 3 second he scroll up and down, it continue until you click to "No" in the popup.

![Use example](imgs/sikulix_jupyter_colab.gif)

## Requirements
* [Java](https://openjdk.java.net/)
* [Sikulix](https://raiman.github.io/SikuliX1/downloads.html)

## How to run
* run sikulix, double click on the .jar file or by command line with:
```
java -jar sikulixide-2.0.5.jar
# or specify the path of your java if you have multiple
/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/bin/java -jar sikulixide-2.0.5.jar
```
* copy into the ide the code at [SikulixScript.py](SikulixScript.py)
* click on run button

